package cz.vse.java.xvalm00.adventuracv.gui;

import cz.vse.java.xvalm00.adventuracv.logika.HerniPlan;
import cz.vse.java.xvalm00.adventuracv.logika.IHra;
import cz.vse.java.xvalm00.adventuracv.util.ObserverZmeny;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;

/**
 * TODO prejmenovat na neco lepsiho, at se neplete s HerniPlan.java .
 */
public class HerniPlocha implements ObserverZmeny {

    private AnchorPane anchorPane = new AnchorPane();
    private Circle aktualniPozice;
    private HerniPlan herniPlan;

    public HerniPlocha(HerniPlan herniPlan){
        this.herniPlan = herniPlan;
        init();
        herniPlan.zaregistrujPozorovatele(this);
    }

    private void init() {
        ImageView herniPlanImageView = new ImageView(new Image(HerniPlocha.class.getResourceAsStream
                ("/zdroje/herniPlan.png"), 400, 250, false, false));
        aktualniPozice = new Circle(10, Paint.valueOf("red"));

//        herniPlan.setAktualniProstor(herniPlan.getAktualniProstor().vratSousedniProstor("les"));

        nastavPoziciHrace();

        anchorPane.getChildren().addAll(herniPlanImageView, aktualniPozice);
    }

    public void nastavPoziciHrace() {
        AnchorPane.setTopAnchor(aktualniPozice, herniPlan.getAktualniProstor().getPosTop());
        AnchorPane.setLeftAnchor(aktualniPozice, herniPlan.getAktualniProstor().getPosLeft());
    }

    public AnchorPane getAnchorPane() {
        return anchorPane;
    }

    @Override
    public void aktualizuj() {
        nastavPoziciHrace();
    }

    public void novaHra(IHra hra) {
        this.herniPlan = hra.getHerniPlan();
        aktualizuj();
    }
}
