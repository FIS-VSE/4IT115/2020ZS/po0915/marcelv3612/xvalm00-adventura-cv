package cz.vse.java.xvalm00.adventuracv.gui;

import cz.vse.java.xvalm00.adventuracv.logika.Batoh;
import cz.vse.java.xvalm00.adventuracv.logika.IHra;
import cz.vse.java.xvalm00.adventuracv.util.ObserverZmeny;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;

import java.io.InputStream;
import java.util.Set;

/**
 * Reprezentuje v grafickem rozhrani panel pro zobrazeni vsech predmetu z batohu.
 */
public class PanelBatohu implements ObserverZmeny {

    private VBox vbox = new VBox();
    private FlowPane panelVeci = new FlowPane();
    private Batoh batoh;

    public PanelBatohu(Batoh batoh) {
        this.batoh = batoh;
        init();
        batoh.zaregistrujPozorovatele(this);
    }

    private void init() {
        vbox.setPrefWidth(100);
        Label label = new Label("Veci v batohu:");
        vbox.getChildren().addAll(label, panelVeci);

        nactiObrazkyVeci();
    }

    private void nactiObrazkyVeci() {
        panelVeci.getChildren().clear();
        String nazevObrazku;
        Set<String> mnozinaVeci = batoh.getMnozinaVeci();
        for (String vec : mnozinaVeci) {
            nazevObrazku = "/zdroje/" + vec + ".jpg";
            InputStream inputStream = PanelBatohu.class.getResourceAsStream(nazevObrazku);
            Image image = new Image(inputStream, 100, 100, false, false);
            ImageView imageView = new ImageView(image);
            panelVeci.getChildren().add(imageView);
        }
    }

    @Override
    public void aktualizuj() {
        nactiObrazkyVeci();
    }

    public Node getPanel() {
        return this.vbox;
    }

    public void novaHra(IHra hra) {
        this.batoh = hra.getBatoh();
        aktualizuj();
    }
}
