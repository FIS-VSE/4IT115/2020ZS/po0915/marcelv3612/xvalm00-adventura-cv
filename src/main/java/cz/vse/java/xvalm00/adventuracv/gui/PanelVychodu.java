package cz.vse.java.xvalm00.adventuracv.gui;

import cz.vse.java.xvalm00.adventuracv.logika.HerniPlan;
import cz.vse.java.xvalm00.adventuracv.logika.IHra;
import cz.vse.java.xvalm00.adventuracv.logika.Prostor;
import cz.vse.java.xvalm00.adventuracv.util.ObserverZmeny;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ListView;


public class PanelVychodu implements ObserverZmeny {

    HerniPlan herniPlan;
    ListView<String> listView = new ListView<>();
    ObservableList<String> vychody = FXCollections.observableArrayList();


    public PanelVychodu(HerniPlan herniPlan) {
        this.herniPlan = herniPlan;
        init();
        herniPlan.zaregistrujPozorovatele(this);
    }

    private void init() {
        listView.setItems(vychody);
        listView.setPrefWidth(100);

        nactiAktualniVychody();
    }

    private void nactiAktualniVychody() {
        for (Prostor vychod : herniPlan.getAktualniProstor().getVychody()) {
            vychody.add(vychod.getNazev());
        }
    }

    public ListView<String> getListView() {
        return listView;
    }

    @Override
    public void aktualizuj() {
        vychody.clear();
        nactiAktualniVychody();
    }

    public void novaHra(IHra hra) {
        this.herniPlan = hra.getHerniPlan();
        aktualizuj();
    }
}
