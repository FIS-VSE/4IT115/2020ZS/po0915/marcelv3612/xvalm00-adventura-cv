package cz.vse.java.xvalm00.adventuracv.logika;

 

import cz.vse.java.xvalm00.adventuracv.util.ObserverZmeny;
import cz.vse.java.xvalm00.adventuracv.util.SubjektZmeny;

import java.util.*;

/**
 *  Trida Batoh 
 *
 *
 *@author     Alena Buchalcevova
 *@version    z kurzu 4IT101 pro školní rok 2014/2015
 */

public class Batoh implements SubjektZmeny
{
public static final int KAPACITA = 2 ;
private Map<String, Vec> seznamVeci ;   // seznam věcí v
    private final Set<ObserverZmeny> mnozinaPozorovatelu = new HashSet<>();

public Batoh () {
seznamVeci = new HashMap<String, Vec>();
}
 /**
     * Vloží věc do batohu
     *
     *@param  vec  instance věci, která se má vložit
     */
   public void vlozVec (Vec vec) {
       seznamVeci.put(vec.getJmeno(),vec);
       upozorniPozorovatele();
    }
     /**
     * Vrací řetězec názvů věcí, které jsou v batohu

     *@return            řetězec názvů
     */
    public String nazvyVeci () {
        String nazvy = "věci v batohu: ";
        for (String jmenoVeci : seznamVeci.keySet()){
            	nazvy += jmenoVeci + " ";
        }
        return nazvy;
    }
     /**
     * Hledá věc daného jména a pokud je v batohu, tak ji vrátí a vymaže ze seznamu

     *@param  jmeno   Jméno věci
     *@return            věc nebo
     *                   hodnota null, pokud tam věc daného jména není 
     */
    public Vec vyberVec (String jmeno) {
        Vec nalezenaVec = null;
        if (seznamVeci.containsKey(jmeno)) {
            nalezenaVec = seznamVeci.get(jmeno);
            seznamVeci.remove(jmeno);
        }
        upozorniPozorovatele();
        return nalezenaVec;
    }

    boolean jePlny() {
        return !(seznamVeci.size() < KAPACITA);
    }

    public Set<String> getMnozinaVeci() {
        return Collections.unmodifiableSet(seznamVeci.keySet());
    }

    @Override
    public void zaregistrujPozorovatele(ObserverZmeny pozorovatel) {
        mnozinaPozorovatelu.add(pozorovatel);
    }

    @Override
    public void odregistrujPozorovatele(ObserverZmeny pozorovatel) {
        mnozinaPozorovatelu.remove(pozorovatel);
    }

    @Override
    public void upozorniPozorovatele() {
        for (ObserverZmeny observerZmeny : mnozinaPozorovatelu) {
            observerZmeny.aktualizuj();
        }

    }
}



