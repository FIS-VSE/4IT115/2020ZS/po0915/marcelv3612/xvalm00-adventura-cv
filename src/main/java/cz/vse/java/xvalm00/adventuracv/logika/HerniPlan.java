package cz.vse.java.xvalm00.adventuracv.logika;


import cz.vse.java.xvalm00.adventuracv.util.ObserverZmeny;
import cz.vse.java.xvalm00.adventuracv.util.SubjektZmeny;

import java.util.HashSet;
import java.util.Set;

/**
 *  Class HerniPlan - třída představující mapu a stav adventury.
 * 
 *  Tato třída inicializuje prvky ze kterých se hra skládá:
 *  vytváří všechny prostory,
 *  propojuje je vzájemně pomocí východů 
 *  a pamatuje si aktuální prostor, ve kterém se hráč právě nachází.
 *
 *@author     Michael Kolling, Lubos Pavlicek, Jarmila Pavlickova, Alena Buchalcevova
 *@version    z kurzu 4IT101 pro školní rok 2014/2015
 */
public class HerniPlan implements SubjektZmeny {
    
    private Prostor aktualniProstor;
    private Prostor viteznyProstor;
    private final Set<ObserverZmeny> mnozinaPozorovatelu = new HashSet<>();


    /**
     *  Konstruktor který vytváří jednotlivé prostory a propojuje je pomocí východů.
     *  Jako výchozí aktuální prostor nastaví halu.
     */
    public HerniPlan() {
        zalozProstoryHry();

    }

    @Override
    public void zaregistrujPozorovatele(ObserverZmeny pozorovatel) {
        mnozinaPozorovatelu.add(pozorovatel);
    }

    @Override
    public void odregistrujPozorovatele(ObserverZmeny pozorovatel) {
        mnozinaPozorovatelu.remove(pozorovatel);
    }

    @Override
    public void upozorniPozorovatele() {
        for (ObserverZmeny observerZmeny : mnozinaPozorovatelu) {
            observerZmeny.aktualizuj();
        }
    }

    public enum Prostory {
        DOMECEK("domeček"), LES("les"), HLUBOKY_LES("hluboký_les"),
        CHALOUPKA("chaloupka"), JESKYNE("jeskyně");

        public final String nazev;

        Prostory(String nazev) {
            this.nazev = nazev;
        }

        public static <T extends Enum<T>> T prostor(String name) {
            T result = null;

            for (Prostory value : Prostory.values()) {
                if (value.nazev.equals(name)) {
                    result = (T) value;
                }
            }

            if (result != null)
                return result;
            if (name == null)
                throw new NullPointerException("Name is null");
            throw new IllegalArgumentException(
                    "No enum constant HerniPlan.Prostory." + name);
        }

    }

    /**
     *  Vytváří jednotlivé prostory a propojuje je pomocí východů.
     *  Jako výchozí aktuální prostor nastaví domeček.
     */
    private void zalozProstoryHry() {
        // vytvářejí se jednotlivé prostory
        Prostor domecek = new Prostor("domeček","domeček, ve kterém bydlí Karkulka", 120.0, 60.0);
        Prostor les = new Prostor("les","les s jahodami, malinami a pramenem vody", 80.0, 145.0);
        Prostor hlubokyLes = new Prostor("hluboký_les","temný les, ve kterém lze potkat vlka", 180.0, 60.0);
        Prostor chaloupka = new Prostor("chaloupka", "chaloupka, ve které bydlí babička Karkulky", 220.0, 40.0);
        Prostor jeskyne = new Prostor("jeskyně","stará plesnivá jeskyně", 180.0, 120.0);
        
        // přiřazují se průchody mezi prostory (sousedící prostory)
        domecek.setVychod(les);
        les.setVychod(domecek);
        les.setVychod(hlubokyLes);
        hlubokyLes.setVychod(les);
        hlubokyLes.setVychod(jeskyne);
        hlubokyLes.setVychod(chaloupka);
        jeskyne.setVychod(hlubokyLes);
        chaloupka.setVychod(hlubokyLes);
                
        aktualniProstor = domecek;  // hra začíná v domečku  
        viteznyProstor = chaloupka ;
        les.vlozVec(new Vec("maliny", true));
        les.vlozVec(new Vec("strom", false));  
    }
    
    /**
     *  Metoda vrací odkaz na aktuální prostor, ve ktetém se hráč právě nachází.
     *
     *@return     aktuální prostor
     */
    
    public Prostor getAktualniProstor() {
        return aktualniProstor;
    }
    
    /**
     *  Metoda nastaví aktuální prostor, používá se nejčastěji při přechodu mezi prostory
     *
     *@param  prostor nový aktuální prostor
     */
    public void setAktualniProstor(Prostor prostor) {
        aktualniProstor = prostor;
        upozorniPozorovatele();
    }
    /**
     *  Metoda vrací odkaz na vítězný prostor.
     *
     *@return     vítězný prostor
     */
    
    public Prostor getViteznyProstor() {
        return viteznyProstor;
    }


}
