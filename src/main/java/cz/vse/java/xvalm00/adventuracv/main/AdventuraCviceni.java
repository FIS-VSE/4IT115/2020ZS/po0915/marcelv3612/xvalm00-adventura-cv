package cz.vse.java.xvalm00.adventuracv.main;

import cz.vse.java.xvalm00.adventuracv.gui.HerniPlocha;
import cz.vse.java.xvalm00.adventuracv.gui.PanelBatohu;
import cz.vse.java.xvalm00.adventuracv.gui.PanelVychodu;
import cz.vse.java.xvalm00.adventuracv.logika.*;
import cz.vse.java.xvalm00.adventuracv.uiText.TextoveRozhrani;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

import static cz.vse.java.xvalm00.adventuracv.logika.HerniPlan.Prostory.*;

public class AdventuraCviceni extends Application {

    private Button tlacitko;
    private FlowPane spodniFlowPane;
    private Label zadejPrikazLabel;
    private TextField prikazovePole;
    private IHra hra = new Hra();
    private TextArea herniKonzole;
    private final HerniPlocha herniPlocha = new HerniPlocha(hra.getHerniPlan());

    private final HerniPlan herniPlan = hra.getHerniPlan();
    private PanelVychodu panelVychodu;
    private PanelBatohu panelBatohu;

    public static void main(String[] args) {
        if (args.length == 0) {
            launch(args);
        } else if (args[0].equals("-text")) {
            IHra hra = new Hra();
            TextoveRozhrani ui = new TextoveRozhrani(hra);
            ui.hraj();
            System.exit(0);
        }
    }

    @Override
    public void start(Stage primaryStage) {

        vytvorTlacitkoProUcitele();
        vytvorHerniKonzoli();
        vytvorSpodniPanel();
        pripravPrikazovePole();

        MenuBar menuBar = vytvorMenu();

        BorderPane hlavniPane = new BorderPane();
        hlavniPane.setCenter(herniKonzole);
        hlavniPane.setBottom(spodniFlowPane);
        hlavniPane.setTop(herniPlocha.getAnchorPane());

        panelVychodu = new PanelVychodu(herniPlan);
        hlavniPane.setRight(panelVychodu.getListView());

        panelBatohu = new PanelBatohu(hra.getBatoh());
        hlavniPane.setLeft(panelBatohu.getPanel());

        VBox hlavniBox = new VBox();
        hlavniBox.getChildren().addAll(menuBar, hlavniPane);

        // Add the layout pane to a scene
        Scene scene = new Scene(hlavniBox, 600, 450);

        // Finalize and show the stage
        primaryStage.setScene(scene);
        primaryStage.setTitle("Aplikace s tlačítkem Zmáčkni Mě");
        prikazovePole.requestFocus();
        primaryStage.show();
    }

    private MenuBar vytvorMenu() {
        Menu menuSoubor = new Menu("Soubor");
        Menu menuNapoveda = new Menu("Napoveda");
        MenuBar menuBar = new MenuBar(menuSoubor, menuNapoveda);

        ImageView novaHraIkonka = new ImageView(new Image(AdventuraCviceni.class.getResourceAsStream("/zdroje/new.gif")));

        MenuItem novaHraMenuItem = vytvorNovaHraMenuItem(novaHraIkonka);
        MenuItem konecMenuItem = new MenuItem("Konec");
        konecMenuItem.setOnAction(t -> System.exit(0));

        menuSoubor.getItems().addAll(novaHraMenuItem, new SeparatorMenuItem(), konecMenuItem);

        MenuItem oAplikaciMenuItem = new MenuItem("O aplikaci");
        MenuItem napovedaMenuItem = new MenuItem("Napoveda");
        menuNapoveda.getItems().addAll(oAplikaciMenuItem, new SeparatorMenuItem(), napovedaMenuItem);

        oAplikaciMenuItem.setOnAction(t -> {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Grafická adventura");
            alert.setHeaderText("JavaFX adventura");
            alert.setContentText("verze LS 2020");
            alert.showAndWait();
        });

        napovedaMenuItem.setOnAction(t -> {
            Stage stage = new Stage();
            stage.setTitle("Nápověda k aplikaci");
            WebView webview = new WebView();
            webview.getEngine().load(AdventuraCviceni.class.getResource("/zdroje/napoveda.htm").toExternalForm());
            stage.setScene(new Scene(webview, 500, 500));
            stage.show();
        });

        return menuBar;
    }

    private MenuItem vytvorNovaHraMenuItem(ImageView novaHraIkonka) {
        MenuItem novaHraMenuItem = new MenuItem("Nova hra", novaHraIkonka);
        novaHraMenuItem.setAccelerator(KeyCombination.keyCombination("Ctrl+N"));

        novaHraMenuItem.setOnAction(t -> {
            hra = new Hra();
            herniKonzole.setText(hra.vratUvitani());
            herniPlocha.novaHra(hra);
            panelVychodu.novaHra(hra);
            panelBatohu.novaHra(hra);
            prikazovePole.requestFocus();
        });
        return novaHraMenuItem;
    }

    private void pripravPrikazovePole() {
        prikazovePole.setOnAction(event -> {
            String zadanyPrikaz = prikazovePole.getText();
            if (zadanyPrikaz.equals("ucitel")) {
                tlacitko.setDisable(false);
            }
            prikazovePole.setText("");

            herniKonzole.appendText("\n" + zadanyPrikaz + "\n");
            String odpovedHry = hra.zpracujPrikaz(zadanyPrikaz);
            herniKonzole.appendText("\n" + odpovedHry + "\n");

            if (hra.konecHry()) {
                prikazovePole.setEditable(false);
            }
        });
    }

    private void vytvorSpodniPanel() {
        spodniFlowPane = new FlowPane();
        zadejPrikazLabel = new Label("Zadej příkaz");
        zadejPrikazLabel.setFont(Font.font("Arial", FontWeight.BOLD, 16));
        prikazovePole = new TextField();
        spodniFlowPane.setAlignment(Pos.CENTER);
        spodniFlowPane.getChildren().addAll (zadejPrikazLabel, prikazovePole, tlacitko);
    }

    private void vytvorHerniKonzoli() {
        herniKonzole = new TextArea();
        herniKonzole.setText(hra.vratUvitani());
        herniKonzole.appendText("\n");
        herniKonzole.setEditable(false);
    }

    private void vytvorTlacitkoProUcitele() {
        tlacitko = new Button();
        tlacitko.setText("Zmáčkni mě.");
        tlacitko.setOnAction(event -> zmacknutoTlacitko());
        tlacitko.setDisable(true);
    }

    private void zmacknutoTlacitko() {

        HerniPlan.Prostory aktualniProstor = HerniPlan.Prostory.prostor(herniPlan.getAktualniProstor().getNazev());

        switch (aktualniProstor) {
            case DOMECEK:
                Prostor sousedniProstor = herniPlan.getAktualniProstor().vratSousedniProstor(LES.nazev);
                automatickyPruchod(sousedniProstor);
                break;
            case LES:
                sousedniProstor = herniPlan.getAktualniProstor().vratSousedniProstor(HLUBOKY_LES.nazev);
                automatickyPruchod(sousedniProstor);
                break;
            case HLUBOKY_LES:
                sousedniProstor = herniPlan.getAktualniProstor().vratSousedniProstor(CHALOUPKA.nazev);
                automatickyPruchod(sousedniProstor);
                break;
        }

    }

    private void automatickyPruchod(Prostor sousedniProstor) {
        String odpovedHry = hra.zpracujPrikaz(PrikazJdi.NAZEV + " " + sousedniProstor.getNazev());
        tlacitko.setText(sousedniProstor.getNazev() + " - jdeme dal?");
        herniKonzole.appendText("\n\n" + odpovedHry + "\n");
    }

}
