package cz.vse.java.xvalm00.adventuracv.util;

/**
 * Tato třída představuje pozorovatele dle návrhového vzoru Observer.
 */
public interface ObserverZmeny {

    /**
     * Metoda, ve které proběhne aktualizace pozorovatele,
     * je volána prostřednictvím metody upozorniPozorovatele z rozhraní SubjektZmeny
     *
     */
    public void aktualizuj();

}
