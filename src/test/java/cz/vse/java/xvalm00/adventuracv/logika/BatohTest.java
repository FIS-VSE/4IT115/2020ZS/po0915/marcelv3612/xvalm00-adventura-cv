package cz.vse.java.xvalm00.adventuracv.logika;

import org.junit.jupiter.api.Test;

import static cz.vse.java.xvalm00.adventuracv.logika.Batoh.KAPACITA;
import static org.junit.jupiter.api.Assertions.*;

class BatohTest {

    @Test
    void jePlny() {
        Batoh batoh = new Batoh();
        assertFalse(batoh.jePlny());
        for (int i = 0; i < KAPACITA ; i++) {
            batoh.vlozVec(new Vec(String.format("maliny %d", i), true));
        }
        assertTrue(batoh.jePlny());
    }
}